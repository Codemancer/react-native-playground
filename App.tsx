import React, { useEffect, useState } from 'react';
import { Alert, Button, StyleSheet, Text, View, Platform } from 'react-native';

export default function App() {
  const [count, setCount] = useState(0)

  const handleSubtract = () => {
    setCount(prev => {
      if (prev > 0) return prev - 1
      else {
        Alert.alert("Cannot count less than zero!")
        return prev;
      };
    })
  }

  const handleAdd = () => { setCount(prev => prev + 1) }

  return (
    <View style={styles.container}>
      <Text>This is running on {Platform.OS + (Platform.Version ? ' ' + Platform.Version : '') }</Text>
      <Text>Incrementer:</Text>
      <Text>{count}</Text>
      <View style={styles.buttonContainer} >
        <Button title="-" onPress={handleSubtract} />
        <Button title="+" onPress={handleAdd} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }, buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%'
  }
});
